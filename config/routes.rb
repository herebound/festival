Rails.application.routes.draw do

  root 'pages#index'

  get 'events/index'

  get 'events/show'

  get 'events/new'

  get 'events/edit'

  get 'events/create'

  get 'events/update'

  get 'events/destroy'

  get 'announcements/new'

  get 'friends/index'

  get 'friends/destroy'

  get '/login', to: 'sessions#new'

  post '/login', to: 'sessions#create'

  put '/logout', to: 'sessions#destroy'

  #custom routes for API requests
  post '/api/login', to: 'api/authentication#authenticate'

  get '/api/events', to: 'api/events#index'

  get '/api/events/:id', to: 'api/events#show'

  get 'api/users/:id', to: 'api/users#show'

  resources :announcements
  resources :events
  resources :groups
  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
