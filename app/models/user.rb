class User < ApplicationRecord
  has_secure_password

  before_save {self.mail = mail.downcase}

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :mail, presence: true, uniqueness: { case_sensitive: false }, format: {with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/, allow_blank: true }
  validates :password, length: { minimum: 8 }, allow_nil: true

  has_many :user_relationships
  has_many :group_memberships
  has_many :groups, through: :group_memberships

end
