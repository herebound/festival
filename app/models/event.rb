class Event < ApplicationRecord
  validates :name, presence: true

  has_many :locations, :autosave => true
  has_many :announcements

  accepts_nested_attributes_for :locations, :allow_destroy => true, :reject_if => :all_blank

end
