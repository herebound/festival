class Location < ApplicationRecord
  validates :name, presence: true
  # LOCATION_TYPE{
  #   :stage    => 0,
  #   :showers  => 1,
  #   :drinks   => 2,
  #   :food     => 3,
  #   :toilet   => 4
  # }
  # belongs_to :maps class_name: "Map", foreign_key: "map_id"
  belongs_to :event
end
