class Group < ApplicationRecord
  validates :name, presence: true
  validates :admin, presence: true

  has_many :group_memberships
  has_many :users, through: :group_memberships
end
