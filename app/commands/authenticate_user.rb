class AuthenticateUser
  prepend SimpleCommand

  def initialize(mail, password)
    @mail = mail
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private
  attr_accessor :mail, :password

  def user
    user = User.find_by(mail: mail)
    return user if user && user.authenticate(password)

    errors.add :user_authentication, 'invalid credentials'
  end

end
