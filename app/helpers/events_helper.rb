module EventsHelper
  def setup_event(event)
    event.locations ||= Location.new
    3.times { event.locations.build }
    event
  end
end
