class AnnouncementsController < ApplicationController
  before_action :set_announcement, only: [:edit, :update, :destroy]

  def new
    @announcement = Announcement.new
    @event = Event.find(params[:event_id])
  end

  def edit
  end

  def create
    @announcement = Announcement.new(announcement_params)
    @event = Event.find(announcement_params[:event_id])

    respond_to do |format|
      if @announcement.save
        format.html { redirect_to @event, notice: 'Announcement was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @event = Event.find(announcement_params[:event_id])
    respond_to do |format|
      if @announcement.update(announcement_params)
        format.html { redirect_to @event, notice: 'Announcement was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @event = Event.find(params[:event_id])
    @announcement.destroy
    respond_to do |format|
      format.html { redirect_to @event, notice: 'Announcement was successfully destroyed.' }
      format.json { render :show, status: :ok, location: @event }
    end
  end

  private

  def set_announcement
    @announcement = Announcement.find(params[:id])
  end

  def announcement_params
    params.require(:announcement).permit(:id, :event_id, :name, :description, :promotion)
  end
end
