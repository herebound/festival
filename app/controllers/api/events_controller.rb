class Api::EventsController < Api::BaseController

  def index
    @events = Event.all
    render json: @events
  end

  def show
    @event = Event.find(params[:id])
    @announcements = Announcement.where(:event_id => params[:id]).reverse
    render json: {event: @event, announcements: @announcements}, except: [:created_at, :updated_at]
  end

end
