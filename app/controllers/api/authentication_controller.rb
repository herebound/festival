class Api::AuthenticationController < Api::BaseController
skip_before_action :authenticate_api_request

def authenticate
  command = AuthenticateUser.call(params[:mail], params[:password])

  if command.success?
    render json: { auth_token: command.result }
  else
    render json: { error: command.errors }, status: :unauthorized
  end
end

end
