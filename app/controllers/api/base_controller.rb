class Api::BaseController < ApplicationController
  respond_to? :json
  skip_before_action :require_login
  skip_before_action :verify_authenticity_token
  before_action :authenticate_api_request
    attr_reader :current_api_user

  def authenticate_api_request
    @current_api_user = AuthenticateApiRequest.call(self.request.headers).result
    render json: { error: 'not authorized' }, status: 401 unless @current_api_user
  end
end
