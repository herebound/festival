class Api::UsersController < Api::BaseController

  def show
    @user = User.find(params[:id])
    render json: @user, except: [:is_admin, :created_at, :updated_at, :password_digest]
  end

  private

  def event_params
    params.require(:event).permit(:id, :name, :description, :date, locations_attributes: [:id, :name, :lat, :long])
  end

end
