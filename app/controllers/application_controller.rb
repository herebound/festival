class ApplicationController < ActionController::Base
  before_action :require_login
  protect_from_forgery with: :exception, unless: :is_json_header
  include SessionsHelper

  private

  def require_login
    unless logged_in?
      flash[:error]= 'Please log in to acces this site.'
      redirect_to login_path
    end
  end

  #used to prevent protection, if a json request (from mobile app) is made
  def is_json_header
    request.format.json?
  end
end
