class SessionsController < ApplicationController
  skip_before_action :require_login

  layout false
  def new
    if current_user
      redirect_to root_path
    else
      render :layout => false
    end
  end

  def create
    user = User.find_by(mail: params[:mail].downcase)
    if user && user.authenticate(params[:password]) && user.is_admin
      log_in user
      redirect_to root_path
    elsif user && user.authenticate(params[:password])
      flash[:error]= 'You are not an Admin!'
      render :new
    else
      flash[:error]= 'Invalid Email or Password!'
      render :new
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
