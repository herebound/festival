json.extract! group, :id, :id, :name, :admin_id, :created_at, :updated_at
json.url group_url(group, format: :json)
