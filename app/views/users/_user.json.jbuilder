json.extract! user, :id, :id, :name, :first_name, :mail, :password, :is_admin, :group_id, :created_at, :updated_at
json.url user_url(user, format: :json)
