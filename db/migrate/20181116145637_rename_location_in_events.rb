class RenameLocationInEvents < ActiveRecord::Migration[5.1]
  def change
    rename_column :events, :location, :adress
  end
end
