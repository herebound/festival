class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :last_name, null: false
      t.string :first_name, null: false
      t.string :mail, null: false
      t.string :crypted_password, null: false
      t.string :password_salt, null: false
      t.boolean :is_admin, default: false

      t.timestamps
    end
  end
end
