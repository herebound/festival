class AlterUserTable < ActiveRecord::Migration[5.1]
  def self.up
    remove_column :users, :password_salt
    remove_column :users, :crypted_password

    add_column :users, :password_digest, :string
  end

  def self.down
    add_column :users, :password_salt, :string
    add_column :users, :crypted_password, :string

    remove_column :users, :password_digest
  end
end
