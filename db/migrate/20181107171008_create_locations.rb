class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.references :event, index: true, foreign_key: true, null: false
      t.string :name
      t.string :lat
      t.string :long
      t.string :type

      t.timestamps
    end
  end
end
