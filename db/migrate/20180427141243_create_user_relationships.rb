class CreateUserRelationships < ActiveRecord::Migration[5.0]
  def change
    create_table :user_relationships do |t|
      t.integer :inviter, index: true, foreign_key: true
      t.integer :invited, index: true, foreign_key: true
      t.integer :type, :default => 0
      t.integer :position_sharing, :default => 0

      t.timestamps
    end
  end
end
