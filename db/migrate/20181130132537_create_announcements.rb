class CreateAnnouncements < ActiveRecord::Migration[5.1]
  def change
    create_table :announcements do |t|
      t.bigint :event_id
      t.string :name
      t.string :description
      t.boolean :promotion

      t.timestamps
    end
  end
end
